import re

input_string = "-2p^3-3p-17"
output_string = ""
poly = re.split("[+-]", input_string)
poly.reverse()
for i in poly:
    if input_string.index(i) == 0 and i != "":
        output_string += '+'
    elif i != "":
        output_string += input_string[input_string.index(i) - 1]
        output_string += i
print(output_string)